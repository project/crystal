CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------

Crystal: A Webpack based Drupal starter theme.
This is an Drupal starter theme that uses Webpack, and is meant to give any
front-end developers a fast kickoff of there project.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/crystal

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/crystal



REQUIREMENTS
------------

A working version of NPM.
See https://docs.npmjs.com/downloading-and-installing-node-js-and-npm for more
details.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal theme. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-themes for
   further information.


CONFIGURATION
-------------

None.


MAINTAINERS
-----------

Current maintainers:
* Dennis Cohn - https://www.drupal.org/user/429431
