const path = require('path');
const _MiniCssExtractPlugin = require('mini-css-extract-plugin');
const _StyleLintPlugin = require('stylelint-webpack-plugin');
const _postcssCustomMedia = require('postcss-custom-media');

const MiniCssExtractPlugin = new _MiniCssExtractPlugin({
  filename: 'css/[name].css',
  chunkFilename: '[id].css',
  path: __dirname + '/dist/css'
});

const StyleLintPlugin = new _StyleLintPlugin({
  configFile: path.resolve(__dirname, 'stylelint.config.js'),
  context: path.resolve(__dirname, '../src/css'),
  files: '**/*.css',
  failOnError: false,
  quiet: false,
});

const postcssCustomMedia = new _postcssCustomMedia({});

module.exports = {
  MiniCssExtractPlugin: MiniCssExtractPlugin,
  StyleLintPlugin: StyleLintPlugin,
  postcssCustomMedia: postcssCustomMedia
};