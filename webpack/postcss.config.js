module.exports = {
  plugins: {
    'postcss-import': {},
    'postcss-nested':{},
    'postcss-custom-media': {},
    'postcss-preset-env': {
      browsers: 'last 2 versions',
    },
    'cssnano': {},
  },
};